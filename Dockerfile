FROM node:19-alpine
ADD . /app
WORKDIR /app
RUN yarn install
RUN yarn build
ENTRYPOINT node .output/server/index.mjs
EXPOSE 3000/tcp
